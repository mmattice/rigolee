#!/bin/sh -eu
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0+

set -eu

COMMANDS=" \
    busybox \
    dumpimage \
    dtc \
    extract-ikconfig \
    debugfs \
    python3 \
    openssl \
"


check_commands()
{
    for cmd in ${COMMANDS}; do
        PATH="${PATH}:/sbin:/usr/sbin:/usr/local/sbin" command -V "${cmd}" || result=1
    done
}

main()
{
    result=0

    check_commands

    if [ "${result}" -ne 0 ]; then
        echo "ERROR: Missing preconditions, cannot continue."
        exit 1
    fi

    echo "All Ok"
}

main "${@}"

exit 0
