# Rigol platform analysis

## Introduction

The Rigol MSO5000 (Kerstrel) and MSO7000 (Flamingo) series platform is based
around the Zynq-7000 series of System on Chip (SoC). The firmware seems to be
brought up using the Xilinx [Open Source Linux (OSL)](http://xilinx.wikidot.com/open-source-linux).

A give-away that the firmware may be based on Xilinx OSL is the fact that the
[password file](http://xilinx.wikidot.com/zynq-rootfs) was initially generated the same way.

Just looking at the wiki it also becomes clear the methods and tools are quite
old. While the zynq-7000 is quite old (2012), it is reasonably well supported with
the main-line tools, such as u-boot, the linux kernel, gcc etc. The wiki uses
extremely old tools (gcc 4.8; 2013), old u-boot (2014) and a very old linux
kernel (3.12; 2013) created by [buildroot 2012.05](firmware/rootfs/etc/os-release).

## Firmware
The Rigol platform seems to be using very similar, if not an identical firmware
build. It uses the typical u-boot + linux stack.

### Buildroot
The compile the entire OS, [buildroot](https://www.buildroot.org) 2012.05 was used.
There are probably no modifications made to buildroot itself, just configurations
added to create their own OS. This does not include the FSBL, which is built
using Vivado.

### First Stage Boot Loader (FSBL)
When the zynq processor starts up, after executing its internal BootROM, the
first code it loads is called the FSBL. This can be either traditionally be
generated from Xilinx's Vivado, or generally is U-Boot's Secondary Program
Loader (SPL). In the case of the Rigol scope, the vivado FSBL was used to
chainload u-boot. The FSBL will load the full u-boot from the QSPI flash into
OCM and start execution there. The FSBL does not know or use the NAND flash, as
it conflicts with the QPSI pins used.

### U-Boot
[Das U-Boot](https://www.denx.de/wiki/U-Boot) v2014.01 from xilinx serves as the
bootloader to prepare the PL part of the zynq and launch linux. It has no
knowledge of the QSPI flash and only can talk to the NAND flash chip.

U-Boot takes as its input a FIT image which contains the Linux kernel, the
flattened device tree and the ramdisk based rootfs as an initrd. The FIT image
contains sha1 integrity hashes for all included components.

U-Boot reads/stores its environment raw in /dev/mtd0 (nand flash offset 0x0) and
is 256 KiB large. The environment is not stored redundantly.

### Linux
The used [linux kernel](https://www.kernel.org) is the 3.12 from xilinx without any additional patches
according to `uname -a`. Of course this is very unlikely as there are added
drivers to begin with, and surely some modifications. According to the extracted
[kernel config](firmware/kerstrel.config), LOCALVERSION_AUTO is set, but that will only work if
an installed git client. So chances are, no version control is used for the
kernel.

#### Non-standard modules
Strangely there are quite a few 3.6.0 kernels installed in the initrd, but its
likely that these are old build artifacts that keep lingering on the host.
The modules directory for the 3.12 kernel is empty. Additional kernel modules
that are being used are strangely stored in [/rigol/drivers](firmware/rootfs/rigol/drivers) and are manually
insmod-ed during startup.

### Busybox
When linux finishes booting, it hands over control the init, which is init from
[busybox](https://www.busybox.net) v1.23.0 and appears to be unmodified.

#### Init
When busybox is given control from the kernel, it executes the executable scripts
as been told via inittab. The buildroot installed scripts from [/etc/init.d/](firmware/rootfs/etc/init.d/)
do not seem to be executed, as they should be called from [rcS](firmware/rootfs/etc/init.d/rcS).
In rcS some standard cleanup and startup steps are performed and a init.d scripts
are re-implemented of sorts (starting mdev for example).

Some interesting Rigol performed changes are that they dump the
environment from mtd0 to /tmp/env.bin. It's an odd place to satisfy the needing
of the env.bin dependency for various scripts they use. Further more, why not
just use u-boot's fw_printenv and fw_saveenv is troublesome.

Next, the application storage is chosen, either mtd6 or mtd10 based on whether
the u-boot variable bootpart reads A or B is mounted as ubi6. This happens with
a silenced kernel printk for some reason.

Finally, the application start.sh script is run to start the main application.

### Application
The main application is started via [start.sh](firmware/rootfs/rigol/shell/start.sh).

#### Startup script
This shell script sets up some environment variables to find and configure Qt5.5.
What's curious is they also manually inject modules from the start script,
rather then relying on properly setup devicetrees to automatically probe the
modules.

One peculiar thing is that the startup script initially calls cfger to generate
the file '/tmp/sysinfo.txt'. Peculiar as the text file only contains the
following u-boot environment variables.
* vendor
* softver
* bootver
* builddate
While the main application also seems to use this file, the [send_email.sh](firmware/rootfs/rigol/shell/send_email.sh)
script initially attached this file to the e-mails it was sending.

What else is curious, that the start script has some hackish SPI binaries which
are used to initialize various remote chips from userspace.
* spi2pll	- probably sets up an external PLL chip
* spi2k7	- initializes the K7 chip which takes 8 seconds!

Finally, after starting the main application 'appEntry' rpcbind, lighthttpd and
cups are started to offer remote features such as LXI and a webpage. It begs to
wonder why these are not init.d initiated scripts.

## Updating
Whenever a GEL file is found on a USB stick the [update.sh](firmware/rootfs/shell/update.sh) is executed
with the location of the GEL file as parameter.

The allowed filenames for the GEL file are:
* DS5000Update.GEL
* DS7000Update.GEL
* DS8000Update.GEL
* DS9000Update.GEL

### From linux
The 'update.sh' script is rather trivial in that it extracts [fw4linux.sh](firmware/fw4linux.sh)
and performs an AES-128-CBC decryption and then executes this now decrypted
script with a GEL file as parameter. The 'fw4linux.sh' script determines the
'bootpart' variable from the u-boot environment and then writes the update to
the opposite locations of the storage. So if bootpart is 'A', then the update
is written to locations 'B', and the boot sequence is changed to boot from 'B'
next reboot.

### From u-boot
There also exists a script that is run from within U-Boot. It has not yet been
observed how this is invoked. Signs have been found within the u-boot binary
that can execute this script if it is found on a USB stick. It is so far unknown
how this is determined.

# Tools
This repository contains some tools to extract GEL files, which optionally can
be run in Docker.

## GEL unpacker

### Docker
There are two ways to the `gel_unpack.sh` script. It is possible to just pull
the container and run the script in the container. One caveat here is however
that the GEL file and the output directory need to be volume mounts. So
for example if we have DS5000Update.GEL file in '/tmp/rigolee/', we can volume mount this
directory, where also the output directory will be created.
```sh
docker run --rm -it -v "/tmp/rigolee:/workdir/" /workdir/ "registry.gitlab.com/riglol/rigolee:latest" -o outdir DS5000Update.GEL
```
After running this container, the GEL file '/tmp/rigolee/DS5000Update.GEL' will be
extracted to '/tmp/rigolee/outdir/'.

### Naively 
Docker is not required to run the script, but it makes it easier without having
to install all dependencies. To check if the used system is compatible, the script
`buildenv_check.sh` can be run. If all is okay, call the script with a GEL file
and an output directory. For example using DS5000Update.GEL and want to have all
output in 'outdir'.
```sh
gel_unpack.sh -o outdir DS5000Update.GEL
```

## GEL packer
To create an update archive, the 'gel_pack.sh' script was created. It takes
multiple files and directories as argument and all adds them to a GEL file.
Do not add the 'fw4linux.sh' or 'fw4uboot.sh' files, these are to be passed
via the '-l' and '-u' parameters instead as these files need to be encrypted.
See `gel_pack.sh -h` for more details.

## QSPI dump unpacker
A simple script has been added to extract the basics from a QSPI flash dump.
The addresses are hardcoded to current known values, so except for extracting
u-boot and the FSBL, nothing is extracted yet. Even the FSBL may be to large
at the moment, but parsing the FSBL header should solve this. Usage example.
```sh
qspi_unpack.sh -o outdir qspi_16mb_dump.bin
```

## Scripts to run on the scope
In the diretory 'scope_scripts' a few scripts can be found that are to be run
directly on the scope.

### Flash switcher
This is a small shell script (to be run on the target) to switch between the two
flash partitions, inspired by the standard 'fw4linux.sh script.' Easiest is to
copy it onto a USB stick and run it directly from the USB stick. So for example,
if the USB drive gets internally mounted to '/media/sda1' (which is the default)
then run as follows:
```sh
sh /media/sda1/flash_switcher.sh A
```
to switch the internal boot to drive A.

Without a parameter, the non-active flash partition will be updated (according
to bootparm from the uboot environment).

Alternatively the `gen_pack.sh` script can be used with the flash switcher script
as its -l parameter.

### Data backup
To be able to easily create a backup from the scope unique data partition, the
script `data_backup.sh` can be used to create a backup on an USB drive. It
requires that a directory named 'data_backup' exists and is writeable on the USB
drive. A dated archive will be created there.

Alternatively the `gen_pack.sh` script can be used with the flash switcher script
as its -l parameter.

# Note
Contrary to normal version control, this repository may receive forced updates.
This because it is more important to have nice historical diff-ability of files
in the firmware.
