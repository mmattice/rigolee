#!/bin/sh -eu
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright 2019 (C) Olliver Schinagl <oliver@schinagl.nl>
#

set -eu

AES_KEY="BAD8CFFEBBAAB5C4C3D8D4BFCAFDBEDD"
GELDIR_TEMPLATE="gelpacker"
KEEPGELDIR=0

PATH="${PATH}:/sbin:/usr/sbin:/usr/local/sbin"


usage()
{
    echo "Uage: ${0} [OPTIONS] FILE [FILE] ..."
    echo "    -d  Device type (required, DS5000 or DS7000)"
    echo "    -h  Print usage"
    echo "    -k  Keep temporary files"
    echo "    -l  Script file to treat as 'f4wlinux.sh'"
    echo "    -u  Script file to treat as 'f4wuboot.sh'"
    echo "FILE(s) are additional files to include in the GEL file"
}

cleanup()
{
    if [ -d "${GELDIR:-}" ] && [ -z "${GELDIR##*${GELDIR_TEMPLATE}*}" ]; then
        if [ "${KEEPGELDIR}" -eq 1 ]; then
            echo "Not deleting '${GELDIR}', please remove manually."
        else
           rm -rf "${GELDIR:?}/"
        fi
    fi
}

main()
{
    while getopts ":d:hkl:u:" options; do
        case "${options}" in
        d)
            DEVICETYPE="${OPTARG}"
            ;;
        h)
            usage
            exit 0
            ;;
        k)
            KEEPGELDIR=1
            ;;
        l)
            FW4LINUX="${OPTARG}"
            ;;
        u)
            FW4UBOOT="${OPTARG}"
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    case "${DEVICETYPE:-}" in
    *"5000"*)
        GELFILE="DS5000Update.GEL"
        ;;
    *"7000"*)
        GELFILE="DS7000Update.GEL"
        ;;
    *)
        GELFILE="UNKNOWNUpdate.GEL"
        ;;
    esac

    if [ -e "${GELFILE}" ]; then
        echo "The file '${GELFILE}' already exists, please remove and try again."
        exit 1
    fi

    GELDIR="$(mktemp -d -t "${GELDIR_TEMPLATE}.XXXXXXXX")"

    if [ "${#}" -gt 0 ]; then
        tar -rf "${GELFILE}" "${@}"
    fi

    if [ -n "${FW4LINUX:-}" ]; then
        cat "${FW4LINUX}" > "${GELDIR}/run.sh"
        openssl aes-128-cbc -in "${GELDIR}/run.sh" -out "${GELDIR}/fw4linux.sh" -e -K "${AES_KEY}" -iv "${AES_KEY}"
        tar -rf "${GELFILE}" -C "${GELDIR}" "fw4linux.sh"
    fi

    if [ -n "${FW4UBOOT:-}" ]; then
        cat "${FW4UBOOT}" > "${GELDIR}/run.sh"
        openssl aes-128-cbc -in "${GELDIR}/run.sh" -out "${GELDIR}/fw4uboot.sh" -e -K "${AES_KEY}" -iv "${AES_KEY}"
        tar -rf "${GELFILE}" -C "${GELDIR}" "fw4uboot.sh"
    fi

    echo "Successfully created '${GELFILE}'."
}

trap cleanup EXIT
main "${@}"

exit 0
