#!/bin/sh

#echo 'This is mail from Scope' | /rigol/mail/bin/mutt -s 'Oscilloscope data' hexiaohua@rigol.com -a /tmp/snap.bmp
#######################################################
#
#SendMail shell
#Paramaters
# p1 to
# p2 attachment path
# p3 content with model/version/serial/date
#
#Return
# 0-Send OK
# 1-Network disconnected
# 2-Send Failed
#######################################################

MailTo=$1
MailAttach=$2
MailResult=/tmp/mail.txt

cat /dev/null > $MailResult

if [ ! -f $MailAttach ]; then
	echo $3 | /rigol/mail/bin/mutt -s 'RIGOL Oscilloscope data' $MailTo > $MailResult
else
	echo $3 | /rigol/mail/bin/mutt -s 'RIGOL Oscilloscope data' $MailTo -a $MailAttach > $MailResult
fi

size=$(stat -c "%s" $MailResult)
if [ $size -eq 0 ]; then
	echo 'OK'
else
	echo 'Failed'
	exit 1
fi

