update_package=$1

if [ ! -f $update_package ]; then
	echo 'upgrade package not exist'
	exit 1
fi

cd /tmp
update_script=fw4linux.sh
run_script=run.sh
tar -xf $update_package $update_script
if [ $? -eq 0 ]; then
	/rigol/tools/cfger -d $update_script $run_script
	chmod +x $run_script
	/tmp/$run_script $1

	#rm /tmp/$update_script >/dev/null 2>&1
else
	echo 'Read package failed'
	exit 2
fi

